# kantkorpus

Find an extensive documentation in German at: 
- kantkorpus/Documentation/Projektbericht_WCM.pdf


Get project via Docker
https://hub.docker.com/r/gruenerkaktus/kantkorpus
and run it using the following instructions: 

## Using Linux / Mac  

### Run Solr instance

1. download Docker-Image  
`docker pull gruenerkaktus/kantkorpus:kks_solr`

2. start the container  
`docker run -d -t gruenerkaktus/kantkorpus:kks_solr`

3. reach Solr Admin Console in your browser  
    http://172.17.0.2:8983/solr/#/fullindex  
    select *Query*, then click on *Execute Query* and you should see the indexed data
    
    if that does not work, check your docker-IP:  
    `docker inspect --format '{{.NetworkSettings.IPAddress}}' $(docker ps -q)`  
    and try: [your docker-IP]:[solr port]/solr/#/fullindex 
    
4. Further help: https://docs.docker.com/samples/library/solr/  

### Run API 
1. download Docker-Image  
    `docker pull gruenerkaktus/kantkorpus:kks_api`  

2. start container  
    `docker run -p 8080:8080 -i -t gruenerkaktus/kantkorpus:kks_api`  
    If your solr instance is running properly, you should be able to see a list of results for an example query at   
    http://localhost:8080/kantkorpus/suche/vernunft  

### Run GUI
1. open new console
2. pick the frontend image that matches the backend-image you used (-local/ -toolbox)
3. Download the image using   
        `docker pull gruenerkaktus/kantkorpus:kks_gui`  
 
4. Run the container using   
        `docker run -d -p 4200:4200 -t gruenerkaktus/kantkorpus:kks_gui`  
    and open http://localhost:4200/search in your browser  

Happy Searching! 

## Using Windows (with Docker Toolbox)

### Run Solr instance
1. download Docker-Image  
`docker pull gruenerkaktus/kantkorpus:kks_solr`

2. start the container  
`docker run -d -t gruenerkaktus/kantkorpus:kks_solr`

3. reach Solr Admin Console in your browser  
    http://172.17.0.2:8983/solr/#/fullindex  
    select *Query*, then click on *Execute Query* and you should see the indexed data
    
    if that does not work, check your docker-IP:  
    `docker inspect --format '{{.NetworkSettings.IPAddress}}' $(docker ps -q)`  
    and try: [your docker-IP]:[solr port]/solr/#/fullindex 
    
4. Further help: https://docs.docker.com/samples/library/solr/   

### Run API 
1. download Docker-Image   
    `docker pull gruenerkaktus/kantkorpus:backend3-toolbox`  

2. start container  
    `docker run -i -p 18080:8080 -t gruenerkaktus/kantkorpus:backend3-toolbox`  
    If your solr instance is running properly, you should be able to see a list of results for an example query at  
    http://192.168.99.100:18080/kantkorpus/suche/vernunft  

### Run GUI
1. open new console
2. pick the frontend image that matches the backend-image you used (-local/ -toolbox)
3. Download the image using   
        `docker pull gruenerkaktus/kantkorpus:frontend1-toolbox`  

4. Run the container using   
        `docker run -d -p 4200:4200  -t gruenerkaktus/kantkorpus:frontend1-toolbox`  
    and open http://192.168.99.100:4200/search in your browser  

Happy Searching! 


## Advanced Options

### Clean Solr instance
If you choose to run a fresh Solr instance, do: 
1. download Docker-Image  
`docker pull gruenerkaktus/kantkorpus:instance1`

2. start the container  
`docker run -i -t gruenerkaktus/kantkorpus:instance1`  
start and create a core called fullindex    
`docker run -i -t gruenerkaktus/kantkorpus:instance1 solr-create -c fullindex`  

3. reach Solr Admin Console in your browser  
    http://172.17.0.2:8983/solr/#/fullindex  

### Start GUI from your machine 
You may start the GUI from your own machine instead of using the container. This is how: 
1. Node.js (required by Angular)  
    `node -v` should give you 8.x or 10.x.
    if not go to https://nodejs.org/en/download/ and dowload it  
    `npm -v` should give you 6.7 or higher

2. Angular CLI (command line interface)  
    `npm install -g @angular/cli`  

3. Run the app  
    go to project folder `cd KantKorpusFrontend`   
    run and open the app `ng serve --open` or short `ng s -o`   
    this should open a browser and show the app under  http://localhost:4200/search  
    make sure the backend is also running and enjoy browsing the works of Immanuel Kant :)

4. Further help: https://angular.io/guide/quickstart  

