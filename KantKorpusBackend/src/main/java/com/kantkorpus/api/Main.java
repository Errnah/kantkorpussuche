package com.kantkorpus.api;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://0.0.0.0:8080/kantkorpus/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.kantkorpus package
        final ResourceConfig rc = new ResourceConfig().packages("com.kantkorpus.api");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

	public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("\nTry the search at:" + "\n\t%ssuche/vernunft\n\n" 
        		+ "You may add some of the following query parameters:\n"
        		+ "\tstart=<start index> (number)\n"
        		+ "\tsize=<result size> (number)\n"
        		+ "\tcs=<case sensitive> (true, false)\n"
        		+ "\tta=<typos allowed> (number)\n"
        		+ "\tvols=<volumes to search> (number)\n"
        		+ "\tlf=<language filter> (lat, griech, ital, franz, port, span, engl, nl, schwed)\n\n"
        		+ "For example like this:" 
        		+ "\n\t%ssuche/vim?start=0&size=5&cs=false&ta=0&vols=1&vols=3&lf=lat"
        		+ "\n\nHave fun exploring the data.."
        		+ "\n\nHit enter to stop the server\n", BASE_URI, BASE_URI));
        System.in.read();
        server.shutdownNow();
    }
}


