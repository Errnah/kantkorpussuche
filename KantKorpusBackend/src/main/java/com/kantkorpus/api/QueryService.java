package com.kantkorpus.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

public class QueryService { //to remove later
	
	private final static String SOLR_URL = "http://172.17.0.2:8983/solr/fullindex";
	String CONTENT = "content";
	
	public QueryResponse getResults(String term, int start, int size, boolean caseSensitive, int typosAllowed, String language, List<Integer> volumes)  throws SolrServerException, IOException {
		
		SolrClient client = new HttpSolrClient.Builder(SOLR_URL).build();
		
		// build the query
		SolrQuery query = getDefaultQuery();
		query.setStart(start);
		query.setRows(size); // max results returned
			
		// handle language parameter
		if(language.equals("lat") || language.equals("griech") || language.equals("ital") || language.equals("franz") || language.equals("port") || language.equals("span") || language.equals("engl") || language.equals("nl") || language.equals("schwed")) {
			query.addFilterQuery("a_sprache:" + language);
		}
		
		// handle typosAllowed parameter (MUST be called before searchTerm goes into query -> fix?)
		if(typosAllowed > 0) {
			term = handleTypos(term, typosAllowed);
		}
		
		//handle case sensitivity
		setContentCaseSensitity(caseSensitive);
		query.addFacetField("m_volume").setQuery(CONTENT + ":" + term);
		
		
		// handle volumes
		if(volumes.size() > 0) {
			List<String> volumesAsString = new ArrayList<String>(volumes.size());
			for (Integer vol : volumes) { 
				volumesAsString.add(String.valueOf(vol)); 
			}
			String list = String.join(" OR ", volumesAsString);			
			query.addFilterQuery("m_volume:(" + list + ")");
		}
		
		// get the results
		QueryResponse response = client.query(query);
		return response; 
	}
	
	public SolrDocumentList getSiteByRef(String volume, String page) throws SolrServerException, IOException {
		SolrClient client = new HttpSolrClient.Builder(SOLR_URL).build();
		
		// build the query
		SolrQuery query = getDefaultQuery();
		query.setQuery("m_volume:" + volume);
		query.addFilterQuery("m_page:" + page);
		query.addFilterQuery("name:seite");

		// get the results
		QueryResponse response = client.query(query);
		SolrDocumentList resultDocs = response.getResults();
		
		return resultDocs;
	}
	
	private SolrQuery getDefaultQuery() {
		SolrQuery query = new SolrQuery();
		query.setFields(CONTENT, "m_volume", "m_page", "name", "id") // fields are in the result
			.set("defType", "edismax")
			.set("debugQuery", "on")
			.set("hl", true)
			.set("hl.fl", "*")	// fields to be highlighted
			.set("hl.requireFieldMatch", true)
			.set("hl.fragsize", 500)	// text around 1 highlight
			.set("hl.usePhraseHighlighter", true)
			.set("hl.highlightMultiTerm", true)
			.set("hl.simple.pre", "<b>")
			.set("hl.simple.post", "</b>");
		return query;
	}
	
	private void setContentCaseSensitity(boolean caseSensitive) {
		this.CONTENT = caseSensitive ? "cs_content" : "content";	
	}
	
	private String handleTypos(String term, int typosAllowed) {
		if (term.startsWith("\"") && term.endsWith("\"")) {
			term += "~" + typosAllowed;
		} else {
			String[] partialTerms = term.split(" ");
			term = "";
			for(String part : partialTerms) {
				part += "~" + typosAllowed;
				term += part + " ";
			}
		}
		return term;
	}
	
	public void getTotalPagesOfVolumes() throws SolrServerException, IOException {
		List<String> pagesPerVolume = new LinkedList<String>();
		
		for (int i = 1; i <= 23; i++) {
			SolrClient client = new HttpSolrClient.Builder(SOLR_URL).build();
			SolrQuery query = new SolrQuery();
			query.setFields("name", "id", "m_volume", "m_page")
				.setQuery("m_volume:" + i)
				.setStart(0)
				.setRows(1000)
				.addFilterQuery("name:seite");
			
			QueryResponse response = client.query(query);
			List<SolrDocument> results = response.getResults();
			
			if (results.isEmpty()) {
				pagesPerVolume.add("0");
			} 
			else {
				Integer maxPageNr = new Integer(0);
				for (SolrDocument doc : results) {
					String page = doc.getFieldValue("m_page").toString();
					page = page.substring(1, page.length() -1);
					int pageNr;
					try {
						pageNr = Integer.parseInt(page);
					} catch (Exception e) {
						pageNr = 0;
					}
					if(pageNr > maxPageNr) {
						maxPageNr = pageNr;
					}
				}
				pagesPerVolume.add(maxPageNr.toString());
			}
		}
	}
	
}
