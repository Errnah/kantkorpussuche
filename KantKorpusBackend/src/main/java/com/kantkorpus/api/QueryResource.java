package com.kantkorpus.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.kantkorpus.api.QueryService;

@Path("suche")
public class QueryResource {
	
	String CONTENT;

	@GET
	@Path("{term}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResults(@PathParam("term") String term, @DefaultValue("0") @QueryParam("start") int start,
			@DefaultValue("10") @QueryParam("size") int size,
			@DefaultValue("false") @QueryParam("cs") boolean caseSensitive,
			@DefaultValue("0") @QueryParam("ta") int typosAllowed, @DefaultValue("") @QueryParam("lf") String language,
			@QueryParam("vols") List<Integer> volumes) throws Exception {

		System.out.println("\n********************************");
		System.out.println("\nSearchTerm: " + term);
		System.out.println("QueryParams: \n" + "\tCaseSensitive: " + caseSensitive
				+ "\n\tTyposAllowed: " + typosAllowed + "\n\tLanguageFilter: " + language + "\n\tVolumes: " + volumes);

		setContentCaseSensitity(caseSensitive);
		
		QueryService queryService = new QueryService();
		QueryResponse queryResponse = queryService.getResults(term, start, size, caseSensitive, 
				typosAllowed, language, volumes);
		QueryResponse fullResponse = queryService.getResults(term, 0, 10000, caseSensitive, typosAllowed, language, volumes);

		JSONArray results = buildJsonResponse(queryResponse, fullResponse);
		
		System.out.println("\nResults found: " + fullResponse.getResults().getNumFound() 
				+ "\nQuery time: " + fullResponse.getQTime() + " ms"
				+ "\nElapsed time: " + fullResponse.getElapsedTime() + " ms");

		return Response.ok().entity(results.toString()).build();
	}

	private void setContentCaseSensitity(boolean caseSensitive) {
		this.CONTENT = caseSensitive ? "cs_content" : "content";	
	}

	@GET
	@Path("/ref")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSiteByRef(@QueryParam("vol") String volume, @QueryParam("p") String page)
			throws SolrServerException, IOException, JSONException {
		QueryService queryService = new QueryService();
		SolrDocumentList resultDocs = queryService.getSiteByRef(volume, page);

		JSONArray result = new JSONArray();
		result.put(resultDocs);

		return Response.ok().entity(result.toString()).build();
	}

	private JSONArray buildJsonResponse(QueryResponse queryResponse, QueryResponse fullResponse) throws JSONException {
		SolrDocumentList resultDocs = queryResponse.getResults();
		JSONArray finalJA = new JSONArray();
		JSONArray metaJA = new JSONArray();

		JSONObject j_meta = new JSONObject("{ \"resultCount\": \"" + resultDocs.getNumFound() + "\"}");
		metaJA.put(j_meta);

		JSONObject start = new JSONObject("{ \"start\": \"" + resultDocs.getStart() + "\"}");
		metaJA.put(start);

		JSONObject size = new JSONObject("{ \"size\": \"" + resultDocs.size() + "\"}");
		metaJA.put(size);

		JSONArray hitsPerVolume = getHitsPerVolume(queryResponse);
		metaJA.put(hitsPerVolume);
		
		JSONArray cooccurrences = getCooccurrences(fullResponse);
		metaJA.put(cooccurrences);

		queryResponse = addSnippetsToResults(queryResponse);

		finalJA.put(metaJA).put(queryResponse.getResults());

		return finalJA;
	}

	private QueryResponse addSnippetsToResults(QueryResponse queryResponse) {
		SolrDocumentList resultDocs = queryResponse.getResults();
		List<String> snippets = new ArrayList<String>();
		// get a list of highlighted snippets
		for (int i = 0; i < resultDocs.size(); i++) {
			String id = resultDocs.get(i).getFieldValue("id").toString();
			String snippet = getHighlightedText(queryResponse, CONTENT, id);
			snippets.add(snippet);
		}
		// add snippets to corresponding result in queryResponse
		for (int i = 0; i < resultDocs.size(); i++) {
			queryResponse.getResults().get(i).addField("snippet", snippets.get(i));
		}
		return queryResponse;
	}

	private JSONArray getCooccurrences(QueryResponse queryResponse) throws JSONException {
		SolrDocumentList resultDocs = queryResponse.getResults();
		Map<String, Integer> listOfTerms = new HashMap<String, Integer>();

		// get a list of highlighted snippets
		for (int i = 0; i < resultDocs.size(); i++) {
			String id = resultDocs.get(i).getFieldValue("id").toString();
			String snippet = getHighlightedText(queryResponse, CONTENT, id);
			String[] terms = snippet.split(" ");
			BiFunction<String, Integer, Integer> countPlusOne = (k, v) -> v + 1;

			for (String term : terms) {
				// get rid of punctuation
				if (term.endsWith("\\p{Punct}")) {
					term = term.substring(0, term.length() - 1);
				}
				// only upper case words
				if (term.matches("\\b([A-Z]\\w*)\\b")) {
					listOfTerms.computeIfPresent(term, countPlusOne);
					listOfTerms.putIfAbsent(term, 1);
				}
			}

		}
		// build the JSON Array
		JSONArray cooccurrences = new JSONArray();
		Collection<String> keysList = listOfTerms.keySet();
		for (String key : keysList) {
			JSONObject coocJson = new JSONObject("{ \"size\": \"" + listOfTerms.get(key) + "\", " +  "\"text\": \"" + key + "\"}");
			cooccurrences.put(coocJson);
		}
		
		return cooccurrences;
	}

	private String getHighlightedText(final QueryResponse queryResponse, final String fieldName, final String docId) {
		String highlightedText = "";
		Map<String, Map<String, List<String>>> highlights = queryResponse.getHighlighting();
		if (highlights != null && MapUtils.isNotEmpty(highlights.get(docId))) {
			List<String> snippets = highlights.get(docId).get(fieldName);
			if (CollectionUtils.isNotEmpty(snippets)) {
				highlightedText = getFragments(snippets);
			}
		}
		return highlightedText;
	}

	private static final String getFragments(List<String> snippets) {
		StringBuilder fragments = new StringBuilder();
		for (int i = 0; i < snippets.size(); i++) {
			if (i > 0) {
				fragments.append(" [..]");
			}
			fragments.append(snippets.get(i));
		}
		return fragments.toString();
	}

	private JSONArray getHitsPerVolume(QueryResponse response) throws JSONException {
		List<Count> facets = response.getFacetFields().get(0).getValues();
		JSONArray hitsPerVolume = new JSONArray();
		boolean isPresent = false;

		// iteration looking for match in facets for the 23 volumes
		for (int i = 1; i <= 23; i++) {
			isPresent = false;
			// set hitsCount for volume that has any hits, meaning it is a facet
			for (Count facet : facets) {
				String iString = String.valueOf(i);
				if (facet.getName().equals(iString)) {
					JSONObject hpv = new JSONObject("{ \"" + "hits" + "\": \"" + facet.getCount() + "\"}");
					hitsPerVolume.put(hpv);
					isPresent = true;
				}
			}
			// set hitsCount to 0 for volume that has no hits, meaning it has no facet
			if (!isPresent) {
				JSONObject hpv = new JSONObject("{ \"" + "hits" + "\": \"0\"}");
				hitsPerVolume.put(hpv);
			}
		}
		return hitsPerVolume;
	}
	
//	private List<String> getHitsPerPage(QueryResponse queryResponse) {
//	SolrDocumentList resultDocs = queryResponse.getResults();
//	List<String> hitsPerPageList = new ArrayList<String>();
//
//	for (int i = 0; i < resultDocs.size(); i++) {
//		String id = resultDocs.get(i).getFieldValue("id").toString();
//		String resultInfo = queryResponse.getExplainMap().get(id);
//		int beginIndex = resultInfo.indexOf("freq") + 5;
//		int endIndex = resultInfo.indexOf("freq") + 6;
//		String hitsPerPage = resultInfo.substring(beginIndex, endIndex);
//		hitsPerPageList.add(hitsPerPage);
//	}
//	System.out.println("HPP: " + hitsPerPageList);
//	return hitsPerPageList;
//}

//	private List<String> getHitsPerPageOfVolume(QueryResponse queryResponse, String volume) {
//		SolrDocumentList resultDocs = queryResponse.getResults();
//		List<String> hitsPerPageList = new ArrayList<String>();
//
//		for (int i = 0; i < resultDocs.size(); i++) {
//			String id = resultDocs.get(i).getFieldValue("id").toString();
//			String resultInfo = queryResponse.getExplainMap().get(id);
//			int beginIndex = resultInfo.indexOf("freq") + 5;
//			int endIndex = resultInfo.indexOf("freq") + 6;
//			String hitsPerPage = resultInfo.substring(beginIndex, endIndex);
//			hitsPerPageList.add(hitsPerPage);
//		}
//		System.out.println("HPP: " + hitsPerPageList);
//		return hitsPerPageList;
//	}

}
