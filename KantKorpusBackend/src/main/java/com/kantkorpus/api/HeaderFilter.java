package com.kantkorpus.api;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class HeaderFilter implements ContainerResponseFilter {
	
	@Override
	public void filter(final ContainerRequestContext requestContext, final ContainerResponseContext responseContext) throws IOException {
		String requestMethod = requestContext.getMethod();

		switch (requestMethod) {
		case "GET":
			responseContext.getHeaders().add("Allow", "GET, HEAD, OPTIONS");
			break;
		case "PUT":
			responseContext.getHeaders().add("Allow", "PUT, HEAD, OPTIONS");
			break;
		case "POST":
			responseContext.getHeaders().add("Allow", "POST, HEAD, OPTIONS");
			break;
		case "DELETE":
			responseContext.getHeaders().add("Allow", "DELETE, HEAD, OPTIONS");
			break;
		case "HEAD":
			responseContext.getHeaders().add("Allow", "GET, HEAD, OPTIONS");
			break;
		default:
			break;
		}
		
		responseContext.getHeaders().add("Access-Control-Allow-Origin", "http://localhost:4200");
		responseContext.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization"); // Accept, Accept-Language, Content-Language, Content-Type are always allowed
	}
}
