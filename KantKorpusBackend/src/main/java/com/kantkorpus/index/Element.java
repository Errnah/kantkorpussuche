package com.kantkorpus.index;

import java.util.Map;

public class Element {

	String name;
	Map<String, String> attributes;
	String content;
	String page;
	String endpage;
	String volume;

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public Element(String name, Map<String, String> attributes, String content, String page, String endpage, String volume) {
		super();
		this.attributes = attributes;
		this.name = name;
		this.content = content;
		this.page = page;
		this.endpage = endpage;
		this.volume = volume;
	}

	public Element(String name) {
		super();
		this.name = name;
	}

// getters and setters
	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getEndpage() {
		return endpage;
	}

	public void setEndpage(String endpage) {
		this.endpage = endpage;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
