package com.kantkorpus.index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.schema.AnalyzerDefinition;
import org.apache.solr.client.solrj.request.schema.FieldTypeDefinition;
import org.apache.solr.client.solrj.request.schema.SchemaRequest;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.xml.sax.XMLReader;

public class SaxToSolr {

	private final static String SOLR_URL = "http://172.17.0.2:8983/solr/fullindex";//Mind the changing URL (added core to access schema) 

	private static String[] filenames = {
			"kantfiles/1.xml",
			"kantfiles/3.xml",
			"kantfiles/4.xml",
			"kantfiles/8.xml", 
			"kantfiles/9.xml"
			};

	public static void main(String[] args) throws Exception {
		
		
		
		
		SolrClient solrClient = getSolrClient();
		
		prepareSchema(solrClient);
		
		for (String filename : filenames) {
			SaxElementHandler elementHandler = new SaxElementHandler();

			SAXParserFactory spf = SAXParserFactory.newInstance();
			spf.setNamespaceAware(true);
			SAXParser saxParser = spf.newSAXParser();
			XMLReader xmlReader = saxParser.getXMLReader();
			xmlReader.setErrorHandler(new SaxErrorHandler(System.err));
			xmlReader.setContentHandler(elementHandler);
			xmlReader.parse(filename);
			List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
			docList = buildSolrDocuments(elementHandler.getElementList());
			indexSolrDocuments(solrClient, docList);
			docList.clear();
		}
	}

	private final static SolrClient getSolrClient() {
		return new HttpSolrClient.Builder(SOLR_URL).withConnectionTimeout(10000).withSocketTimeout(60000).build();
	}

	private final static List<SolrInputDocument> buildSolrDocuments(List<Element> elements) {
		List<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
		
		//building list of page-elements
		PageHandler pageHandler = new PageHandler();
		for(Element element:elements){
			if(element.getName() == "seite"){
				pageHandler.insertElement(element);
				System.out.println(element.getPage());
			}
		}

		//building solr documents
		for (Element element : elements) {
			SolrInputDocument doc = new SolrInputDocument();

			doc.addField("name", element.getName());
			// prefix "m_" for meta-data pulled from other elements
			doc.addField("m_volume", element.getVolume());
			// add the page to the document and endpage, if element spans over more than one
			// page
			if (!"".equals(element.getPage())) {
				doc.addField("m_page", element.getPage());
			}
			if (element.getEndpage() != null) {
				doc.addField("m_endpage", element.getEndpage());
			}

			// add the attributes to the document (with name-prefix "a_" to avoid
			// collisions).
			// Adding nested documents (addChildDocument()) is buggy, so do not use.
			if (!element.getAttributes().isEmpty()) {
				for (Map.Entry<String, String> attribute : element.getAttributes().entrySet()) {
					doc.addField("a_" + attribute.getKey(), attribute.getValue());
				}
			}

			// add the content to the document
			if (element.getContent() != null && !element.getContent().isEmpty()) {
				doc.addField("content", element.getContent());
			}
			//Adding full page content - intended to make better snippets but temporarily omitted for blowing up index and therefore query processing time immensely
//			doc.addField("page_content", pageHandler.getPageContent(element.getVolume(), element.getPage()) );
			
			docList.add(doc);
		}
		return docList;
	}

	private static void indexSolrDocuments(SolrClient solrClient, List<SolrInputDocument> docList) {
		for (SolrInputDocument doc : docList) {
			// prints what goes to the index
			System.out.println(doc.toString());
			try {
				@SuppressWarnings("unused")
				UpdateResponse updateResponse = solrClient.add(doc);				
				// Indexed documents must be committed
				solrClient.commit();
			} catch (SolrServerException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void prepareSchema(SolrClient solrClient) throws SolrServerException, IOException{
		// Adding field a_nr with correct type to managed_schema to avoid
		// type-casting error
		Map<String, Object> fieldAttributes = new LinkedHashMap<>();
		fieldAttributes.put("name", "a_nr");
		fieldAttributes.put("type", "string");
		SchemaRequest.AddField fixedAnr = new SchemaRequest.AddField(fieldAttributes);
		fixedAnr.process(solrClient);

		// Adding field 'content' so copyfield can be made
		Map<String, Object> contentAttributes = new LinkedHashMap<>();
		contentAttributes.put("name", "content");
		contentAttributes.put("type", "text_general");
		contentAttributes.put("stored", "true");
		contentAttributes.put("indexed", "true");
		SchemaRequest.AddField content = new SchemaRequest.AddField(contentAttributes);
		content.process(solrClient);

		// Adding new field_type similar to text_general for case-sensitive
		// search in 'content'
		FieldTypeDefinition textGeneralCs = new FieldTypeDefinition();
		// field type attributes
		Map<String, Object> fieldTypeAttributes = new LinkedHashMap<>();
		fieldTypeAttributes.put("name", "cs_text_general");
		fieldTypeAttributes.put("class", "solr.TextField");
		fieldTypeAttributes.put("positionIncrementGap", "100");
		fieldTypeAttributes.put("multiValued", "true");
		textGeneralCs.setAttributes(fieldTypeAttributes);
		// analyzers
		AnalyzerDefinition analyzersCs = new AnalyzerDefinition();
		// filters&tokenizer
		Map<String, Object> tokenizer = new LinkedHashMap<String, Object>();
		tokenizer.put("class", "solr.StandardTokenizerFactory");
		analyzersCs.setTokenizer(tokenizer);
		Map<String, Object> stopwords = new LinkedHashMap<String, Object>();
		stopwords.put("class", "solr.StopFilterFactory");
		stopwords.put("words", "stopwords.txt");
		stopwords.put("ignoreCase", "true");
		List<Map<String, Object>> filters = new LinkedList<Map<String, Object>>();
		filters.add(stopwords);
		analyzersCs.setFilters(filters);
		// Attributes
		Map<String, Object> attributesCs = new LinkedHashMap<>();
		attributesCs.put("type", "index");
		analyzersCs.setAttributes(attributesCs);
		System.out.println(analyzersCs.toString()); // Testing print
		textGeneralCs.setAnalyzer(analyzersCs); // Setting analyzerdefinition
		SchemaRequest.AddFieldType addTextCs = new SchemaRequest.AddFieldType(textGeneralCs);
		addTextCs.process(solrClient);

		// Adding field for case-senstive content
		Map<String, Object> caseSensitiveAttributes = new LinkedHashMap<>();
		caseSensitiveAttributes.put("name", "cs_content");
		caseSensitiveAttributes.put("type", "cs_text_general");
		caseSensitiveAttributes.put("stored", "true");
		caseSensitiveAttributes.put("indexed", "true");
		SchemaRequest.AddField contentCs = new SchemaRequest.AddField(caseSensitiveAttributes);
		contentCs.process(solrClient);

		// Adding copyfield for case-sensitive content
		List<String> destination = new LinkedList<String>();
		destination.add("cs_content");
		SchemaRequest.AddCopyField caseSensitiveContent = new SchemaRequest.AddCopyField("content", destination);
		caseSensitiveContent.process(solrClient);
	}
}
