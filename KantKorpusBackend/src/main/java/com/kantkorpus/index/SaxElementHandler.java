package com.kantkorpus.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxElementHandler extends DefaultHandler {

	public List<Element> elementList = new ArrayList<Element>();
	private Stack<Element> openElements = new Stack<Element>();
	private Stack<StringBuffer> contentBuffers = new Stack<StringBuffer>();

	private Element page;
	private StringBuffer pageBuffer;

	private String currentPage = "";
	private String currentVolume = "";

	@Override
//	called at the start of any element
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		// ignore <zeile/> tags for now
		// if needed later process them like <seite/>
		// ignore <trenn/>
		if ("zeile".equals(qName) || "trenn".equals(qName)) {
			return;
		}

		// keep track of currentPage
		if ("seite".equals(qName)) {
			setCurrentPage(atts.getValue(0));
		}

		// keep track of currentVolume
		if ("band".equals(qName)) {
			setCurrentVolume(atts.getValue(0));
		}

		// get attributes of the element
		Map<String, String> attributes = new HashMap<String, String>();
		for (int i = 0; i < atts.getLength(); i++) {
			attributes.put(atts.getQName(i), atts.getValue(i));
		}
		
		// special treatment for <seite/> so that they have content
		if("seite".equals(qName)) {
			handlePage(qName, atts);
			return;
		}	
		
		Element element = new Element(qName);
		element.setVolume(getCurrentVolume());
		element.setPage(getCurrentPage());
		element.setAttributes(attributes);

		openElements.push(element);
		contentBuffers.push(new StringBuffer());
	}

	@Override
//	called at the end of any element
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// ignore <zeile/> tags for now
		// do nothing on end of <seite/> tag
		if ("zeile".equals(qName) || "seite".equals(qName) || "trenn".equals(qName)) {
			return;
		}

		if (!openElements.isEmpty()) {
			Element element = openElements.pop();

			// get content of the element
			if (contentBuffers != null) {
				String elementContent = contentBuffers.pop().toString();
				if (elementContent != null) {
					element.setContent(elementContent.toString());
				}
			}

			// set endpage for elements that span over more than one page
			if (element.getPage() != currentPage) {
				element.setEndpage(currentPage);
			}
			elementList.add(element);
		}
	}

//	reads (plain-text) content between elements
	public void characters(char[] ch, int start, int length) throws SAXException {
		String contentChunk = new String(ch, start, length);

		// ignore white-space-only content-chunks
		if (contentChunk.length() == 0) {
			return;
		}

		// append new content-chunk to all open tags
		for (StringBuffer buf : contentBuffers) {
			buf.append(contentChunk);
		}

		// append new content-chunk to pageBuffer
		if (pageBuffer != null) {
			pageBuffer.append(contentChunk);
		}
	}

	private void handlePage(String qName, Attributes atts) {
		if (page != null) {
			page.setContent(pageBuffer.toString());
			elementList.add(page);
		}

		// get attributes of the element
		Map<String, String> attributes = new HashMap<String, String>();
		for (int i = 0; i < atts.getLength(); i++) {
			attributes.put(atts.getQName(i), atts.getValue(i));
		}
		
		page = new Element(qName);
		pageBuffer = new StringBuffer();
		page.setVolume(currentVolume);
		page.setPage(currentPage);
		page.setAttributes(attributes);
	}
	
	public List<Element> getElementList() {
		return elementList;
	}

	public String getCurrentVolume() {
		return currentVolume;
	}

	public void setCurrentVolume(String currentVolume) {
		this.currentVolume = currentVolume;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

}
