package com.kantkorpus.index;

import java.util.ArrayList;

/**
 * This class is for handling full pages for later use in the search index. 
 * @author kth_m
 *
 */
public class PageHandler {
	/**
	 * List of elements that represent a page
	 */
	private ArrayList<Element> pageList = new ArrayList<Element>();
	
	/**
	 * Get-Method for a specific page in a specific volume
	 * @param volume
	 * @param page
	 * @return
	 */
	public Element getPageElement(String volume, String page){
		Element etemp = new Element("temp");
		for(Element e : pageList){
			if((e.getPage()==page) && (e.getVolume()==volume)){
				etemp = e;
			}
		}
		return etemp;
	}
	/**
	 * getter for content of a page
	 * @param volume
	 * @param page
	 * @return
	 */
	public String getPageContent(String volume, String page){
		String ctemp = "";
		for(Element e : pageList){
			if((e.getPage()==page) && (e.getVolume()==volume)){
				ctemp=e.getContent();
			}
		}
		return ctemp;
	}
	
	/**
	 * Inserting an Element into the list of page-elements
	 * @param e
	 */
	public void insertElement(Element e){
		this.pageList.add(e);
	}
}
