import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-results-list',
  templateUrl: './results-list.component.html',
  styleUrls: ['./results-list.component.css']
})
export class ResultsListComponent implements OnInit {

  searchResults: any;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.searchResultsObservable.subscribe((data: any[]) => this.searchResults = data);
  }

}
