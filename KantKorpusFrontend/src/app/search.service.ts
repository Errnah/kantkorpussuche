import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  apiUrl = 'http://localhost:8080/kantkorpus/';
  searchUrl = 'suche/';
  headers = new HttpHeaders();

  private searchTerm = new BehaviorSubject<string>('');
  searchTermObservable = this.searchTerm.asObservable();

  private typosAllowed = new BehaviorSubject<string>('0');
  typosAllowedObservable = this.typosAllowed.asObservable();

  private caseSensitive = new BehaviorSubject<boolean>(false);
  caseSensitiveObservable = this.caseSensitive.asObservable();

  private languageFilter = new BehaviorSubject<string>('');
  languageFilterObservable = this.languageFilter.asObservable();

  private searchResults = new BehaviorSubject<any[]>(null);
  searchResultsObservable = this.searchResults.asObservable();

  private searchMetaInfo = new BehaviorSubject<any[]>(null);
  searchMetaInfoObservable = this.searchMetaInfo.asObservable();

  private pageIndex = new BehaviorSubject<number>(0);
  pageIndexObservable = this.pageIndex.asObservable();

  private pageSize = new BehaviorSubject<number>(10);
  pageSizeObservable = this.pageSize.asObservable();

  private hitsPerVolume = new BehaviorSubject<any[]>(null);
  hitsPerVolumeObservable = this.hitsPerVolume.asObservable();

  private checkedAll = new BehaviorSubject<boolean>(true);
  checkedAllObservable = this.checkedAll.asObservable();

  private checkedAbt1 = new BehaviorSubject<boolean>(true);
  checkedAbt1Observable = this.checkedAbt1.asObservable();

  private checkedAbt1Bd1 = new BehaviorSubject<boolean>(true);
  checkedAbt1Bd1Observable = this.checkedAbt1Bd1.asObservable();

  private checkedAbt1Bd3 = new BehaviorSubject<boolean>(true);
  checkedAbt1Bd3Observable = this.checkedAbt1Bd3.asObservable();

  private checkedAbt1Bd4 = new BehaviorSubject<boolean>(true);
  checkedAbt1Bd4Observable = this.checkedAbt1Bd4.asObservable();

  private checkedAbt1Bd8 = new BehaviorSubject<boolean>(true);
  checkedAbt1Bd8Observable = this.checkedAbt1Bd8.asObservable();

  private checkedAbt1Bd9 = new BehaviorSubject<boolean>(true);
  checkedAbt1Bd9Observable = this.checkedAbt1Bd9.asObservable();

  private volumesToSearch = new BehaviorSubject<any[]>([1, 3, 4, 8, 9]);
  volumesToSearchObservable = this.volumesToSearch.asObservable();

  private cooccurrences = new BehaviorSubject<any[]>(null);
  cooccurrencesObservable = this.cooccurrences.asObservable();

  constructor(private httpClient: HttpClient) { }

  newSearchTerm(searchTerm: string) {
    this.searchTerm.next(searchTerm);
  }

  newTyposAllowed(typosAllowed: string) {
    this.typosAllowed.next(typosAllowed);
  }

  newCaseSensitive(caseSensitive: boolean) {
    this.caseSensitive.next(caseSensitive);
  }

  newLanguageFilter(languageFilter: string) {
    this.languageFilter.next(languageFilter);
  }

  newSearchResults(searchResults: any[]) {
    this.searchResults.next(searchResults);
  }

  newSearchMetaInfo(searchMetaInfo: any[]) {
    this.searchMetaInfo.next(searchMetaInfo);
  }

  newPageIndex(pageIndex: number) {
    this.pageIndex.next(pageIndex);
  }

  newPageSize(pageSize: number) {
    this.pageSize.next(pageSize);
  }

  newHitsPerVolume(hitsPerVolume: any[]) {
    this.hitsPerVolume.next(hitsPerVolume);
  }

  newCheckedAll(checkedAll: boolean) {
    this.checkedAll.next(checkedAll);
  }

  newCheckedAbt1(checkedAbt1: boolean) {
    this.checkedAbt1.next(checkedAbt1);
  }

  newCheckedAbt1Bd1(checkedAbt1Bd1: boolean) {
    this.checkedAbt1Bd1.next(checkedAbt1Bd1);
  }

  newCheckedAbt1Bd3(checkedAbt1Bd3: boolean) {
    this.checkedAbt1Bd3.next(checkedAbt1Bd3);
  }

  newCheckedAbt1Bd4(checkedAbt1Bd4: boolean) {
    this.checkedAbt1Bd4.next(checkedAbt1Bd4);
  }

  newCheckedAbt1Bd8(checkedAbt1Bd8: boolean) {
    this.checkedAbt1Bd8.next(checkedAbt1Bd8);
  }

  newCheckedAbt1Bd9(checkedAbt1Bd9: boolean) {
    this.checkedAbt1Bd9.next(checkedAbt1Bd9);
  }

  newVolumesToSearch(volumesToSearch: any[]) {
    this.volumesToSearch.next(volumesToSearch);
  }

  newCooccurrences(cooccurrences: any[]) {
    this.cooccurrences.next(cooccurrences);
  }

}
