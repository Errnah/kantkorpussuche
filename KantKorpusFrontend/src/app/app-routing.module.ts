import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainSearchComponent } from './main-search/main-search.component';
import { ResultComponent } from './result/result.component';

const routes: Routes = [
  { path: '', redirectTo: '/search', pathMatch: 'full' },
  { path: 'result/:vol/:p', component: ResultComponent },
  { path: 'search', component: MainSearchComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
