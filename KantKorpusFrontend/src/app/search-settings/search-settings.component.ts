import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-search-settings',
  templateUrl: './search-settings.component.html',
  styleUrls: ['./search-settings.component.css']
})
export class SearchSettingsComponent implements OnInit {

  typosAllowed: string;
  caseSensitive: boolean;
  languageFilter: string; // rename to languageSelected
  checkedAll: boolean;
  checkedAbt1: boolean;
  checkedAbt1Bd1: boolean;
  checkedAbt1Bd3: boolean;
  checkedAbt1Bd4: boolean;
  checkedAbt1Bd8: boolean;
  checkedAbt1Bd9: boolean;
  volumesToSearch = [];

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.typosAllowedObservable.subscribe(data => this.typosAllowed = data);
    this.searchService.caseSensitiveObservable.subscribe(data => this.caseSensitive = data);
    this.searchService.languageFilterObservable.subscribe(data => this.languageFilter = data);
    this.searchService.checkedAllObservable.subscribe(data => this.checkedAll = data);
    this.searchService.checkedAbt1Observable.subscribe(data => this.checkedAbt1 = data);
    this.searchService.checkedAbt1Bd1Observable.subscribe(data => this.checkedAbt1Bd1 = data);
    this.searchService.checkedAbt1Bd3Observable.subscribe(data => this.checkedAbt1Bd3 = data);
    this.searchService.checkedAbt1Bd4Observable.subscribe(data => this.checkedAbt1Bd4 = data);
    this.searchService.checkedAbt1Bd8Observable.subscribe(data => this.checkedAbt1Bd8 = data);
    this.searchService.checkedAbt1Bd9Observable.subscribe(data => this.checkedAbt1Bd9 = data);
    this.searchService.volumesToSearchObservable.subscribe(data => this.volumesToSearch = data);
  }

  onTyposAllowedChange() {
    this.searchService.newTyposAllowed(this.typosAllowed);
  }

  onCaseSensitiveChange() {
    this.searchService.newCaseSensitive(this.caseSensitive);
  }

  onLanguageFilterChange() {
    this.searchService.newLanguageFilter(this.languageFilter);
  }

  onCheckedAllChange() {
    this.searchService.newCheckedAll(!this.checkedAll);
    if (this.checkedAll === true) {
      this.searchService.newCheckedAbt1(true);
      this.searchService.newCheckedAbt1Bd1(true);
      this.searchService.newCheckedAbt1Bd3(true);
      this.searchService.newCheckedAbt1Bd4(true);
      this.searchService.newCheckedAbt1Bd8(true);
      this.searchService.newCheckedAbt1Bd9(true);
      this.searchService.newVolumesToSearch([1, 3, 4, 8, 9]);
    }
  }

  onCheckedAbt1Change() {
    this.searchService.newCheckedAbt1(!this.checkedAbt1);
    this.searchService.newCheckedAbt1Bd1(this.checkedAbt1);
    this.addOrRemoveVolume(this.checkedAbt1Bd1, 1);
    this.searchService.newCheckedAbt1Bd3(this.checkedAbt1);
    this.addOrRemoveVolume(this.checkedAbt1Bd3, 3);
    this.searchService.newCheckedAbt1Bd4(this.checkedAbt1);
    this.addOrRemoveVolume(this.checkedAbt1Bd4, 4);
    this.searchService.newCheckedAbt1Bd8(this.checkedAbt1);
    this.addOrRemoveVolume(this.checkedAbt1Bd8, 8);
    this.searchService.newCheckedAbt1Bd9(this.checkedAbt1);
    this.addOrRemoveVolume(this.checkedAbt1Bd9, 9);
  }

  onCheckedAbt1Bd1Change() {
    this.searchService.newCheckedAbt1Bd1(!this.checkedAbt1Bd1);
    this.addOrRemoveVolume(this.checkedAbt1Bd1, 1);
  }

  onCheckedAbt1Bd3Change() {
    this.searchService.newCheckedAbt1Bd3(!this.checkedAbt1Bd3);
    this.addOrRemoveVolume(this.checkedAbt1Bd3, 3);
  }

  onCheckedAbt1Bd4Change() {
    this.searchService.newCheckedAbt1Bd4(!this.checkedAbt1Bd4);
    this.addOrRemoveVolume(this.checkedAbt1Bd4, 4);
  }

  onCheckedAbt1Bd8Change() {
    this.searchService.newCheckedAbt1Bd8(!this.checkedAbt1Bd8);
    this.addOrRemoveVolume(this.checkedAbt1Bd8, 8);
  }

  onCheckedAbt1Bd9Change() {
    this.searchService.newCheckedAbt1Bd9(!this.checkedAbt1Bd9);
    this.addOrRemoveVolume(this.checkedAbt1Bd9, 9);
  }

  addOrRemoveVolume(checked: boolean, nr: number) {
    if (checked && !this.volumesToSearch.includes(nr)) {
      this.volumesToSearch.push(nr);
    }
    if (!checked && this.volumesToSearch.includes(nr)) {
      this.volumesToSearch.splice(this.volumesToSearch.indexOf(nr), 1);
    }
  }
}
