import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';
import { HttpClient } from '@angular/common/http';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-main-search',
  templateUrl: './main-search.component.html',
  styleUrls: ['./main-search.component.css']
})
export class MainSearchComponent implements OnInit {

  searchTerm: string;
  typosAllowed: string;
  caseSensitive: boolean;
  languageFilter: string;

  searchResults: any;
  searchMetaInfo: any;

  length: number;
  pageIndex: number;
  start: number;
  pageSize: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  hitsPerVolume: any;
  hitsTmp = [];

  cooccurrences: any;
  coocTmp = [];

  volumesToSearch: any;
  volumes: any;

  pageEvent: PageEvent;

  constructor(private searchService: SearchService, private httpClient: HttpClient) { }

  ngOnInit() {
    this.searchService.searchTermObservable.subscribe(term => this.searchTerm = term);
    this.searchService.typosAllowedObservable.subscribe(data => this.typosAllowed = data);
    this.searchService.caseSensitiveObservable.subscribe(data => this.caseSensitive = data);
    this.searchService.languageFilterObservable.subscribe(data => this.languageFilter = data);
    this.searchService.searchResultsObservable.subscribe(data => this.searchResults = data);
    this.searchService.searchMetaInfoObservable.subscribe(data => this.searchMetaInfo = data);
    this.searchService.pageIndexObservable.subscribe(data => this.pageIndex = data);
    this.searchService.pageSizeObservable.subscribe(data => this.pageSize = data);
    this.searchService.hitsPerVolumeObservable.subscribe((data: any[]) => this.hitsPerVolume = data);
    this.searchService.volumesToSearchObservable.subscribe((data: any[]) => this.volumesToSearch = data);
    this.searchService.cooccurrencesObservable.subscribe((data: any[]) => this.cooccurrences = data);

    if (this.searchTerm !== '') {
      this.getResults();
    }
  }

  onSearch() {
    this.searchService.newSearchTerm(this.searchTerm);
    this.start = 0;
    this.pageSize = 10;
    this.pageIndex = 0;
    this.convertVolumes();
    this.getResults();
  }

  getResults(): void {
    this.start = (this.pageIndex * this.pageSize) + 1;
    this.httpClient.get('http://localhost:8080/kantkorpus/suche/' + this.searchTerm + '?'
      + 'start=' + this.start + '&'
      + 'size=' + this.pageSize + '&'
      + 'cs=' + this.caseSensitive + '&'
      + 'ta=' + this.typosAllowed + '&'
      + this.volumes
      + 'lf=' + this.languageFilter + '&'
    )
      .subscribe((data: any[]) => {
        this.searchService.newSearchResults(data[1]);
        this.searchService.newSearchMetaInfo(data[0]);
        this.hitsTmp.length = 0;
        data[0][3].forEach(vol => {
          this.hitsTmp.push(vol.hits);
        });
        this.searchService.newHitsPerVolume(this.hitsTmp);

        this.coocTmp.length = 0;
        data[0][4].forEach(cooc => {
          this.coocTmp.push({ text: cooc.text, size: cooc.size });
        });
        this.searchService.newCooccurrences(this.coocTmp);
      });
  }

  // pagination
  onPageChange(e: any) {
    this.pageIndex = e.pageIndex;
    this.searchService.newPageIndex(this.pageIndex);
    this.pageSize = e.pageSize;
    this.searchService.newPageSize(this.pageSize);
    this.getResults();
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  convertVolumes() {
    this.volumes = '';
    this.volumesToSearch.forEach(vol => {
      this.volumes += 'vols=' + vol + '&';
    });
  }

}
