import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  result: any;
  volume: any;
  page: any;
  pageInt: number;
  originalPage: any;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private httpClient: HttpClient,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'arrow-left',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/icons/outline-chevron_left-24px.svg'));
  }

  ngOnInit(): void {
    this.volume = this.route.snapshot.paramMap.get('vol');
    this.page = this.route.snapshot.paramMap.get('p');
    this.originalPage = this.route.snapshot.paramMap.get('p');
    this.getResult();
  }

  getResult(): void {
    this.httpClient.get('http://localhost:8080/kantkorpus/suche/ref?vol=' + this.volume + '&p=' + this.page).subscribe((data: any[]) => {
      this.result = data[0];
    });
  }

  goBackToSearch(): void {
    this.location.back();
  }

  getPreviousPage(): void {
    this.pageInt = parseInt(this.page, 10);
    this.page = this.pageInt - 1;
    this.getResult();
  }

  getNextPage(): void {
    this.pageInt = parseInt(this.page, 10);
    this.page = this.pageInt + 1;
    this.getResult();
  }

  getOriginalPage(): void {
    this.page = this.originalPage;
    this.getResult();
  }


}
