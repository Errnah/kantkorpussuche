import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatSliderModule,
  MatRadioModule,
  MatChipsModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatGridListModule,
  MatDividerModule,
  MatCardModule,
  MatSelectModule,
  MatOptionModule,
  MatButtonToggleModule,
  MatPaginatorModule,
  MatTabsModule,
  MatTreeModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResultsListComponent } from './results-list/results-list.component';
import { SearchSettingsComponent } from './search-settings/search-settings.component';
import { MainSearchComponent } from './main-search/main-search.component';
import { ResultComponent } from './result/result.component';
import 'hammerjs';
import { SearchService } from './search.service';
import { ResultsDistributionComponent } from './results-distribution/results-distribution.component';
import { ResultsCooccurrencesComponent } from './results-cooccurrences/results-cooccurrences.component';
import { AgWordCloudModule } from 'angular4-word-cloud';

@NgModule({
  declarations: [
    AppComponent,
    ResultsListComponent,
    SearchSettingsComponent,
    MainSearchComponent,
    ResultComponent,
    ResultsDistributionComponent,
    ResultsCooccurrencesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatSliderModule,
    MatRadioModule,
    MatChipsModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatGridListModule,
    MatDividerModule,
    MatCardModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatTabsModule,
    MatTreeModule,
    AgWordCloudModule.forRoot()
  ],
  providers: [SearchService],
  bootstrap: [AppComponent],
  exports: [BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatSliderModule,
    MatRadioModule,
    MatChipsModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatGridListModule,
    MatDividerModule,
    MatCardModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatTabsModule,
    MatTreeModule
  ]
})
export class AppModule { }
