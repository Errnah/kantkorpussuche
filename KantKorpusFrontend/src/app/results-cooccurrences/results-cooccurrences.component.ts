import { Component, OnInit, ViewChild, OnChanges, Input } from '@angular/core';
import { AgWordCloudData, AgWordCloudDirective, AgWordCloudOptions } from 'angular4-word-cloud';
import { SearchService } from '../search.service';

// https://github.com/alhazmy13/Angular4-word-cloud

@Component({
  selector: 'app-results-cooccurrences',
  templateUrl: './results-cooccurrences.component.html',
  styleUrls: ['./results-cooccurrences.component.css']
})
export class ResultsCooccurrencesComponent implements OnInit, OnChanges {
  @ViewChild('wordCloudChart') wordCloudChart: AgWordCloudDirective;
  @Input() searchResults: any;

  wordData: Array<AgWordCloudData> = [];

  options: AgWordCloudOptions = {
    settings: {
      minFontSize: 12,
      maxFontSize: 18,
    },
    margin: {
      top: 10,
      right: 10,
      bottom: 10,
      left: 10
    },
    labels: true, // false to hide hover labels
  };
  width = 1000; // default-height is 0.75 * width
  color = ['#3333ff', '#0099ff', '#006bb3', '#004d80', '#66c2ff', '#3366cc'];

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.cooccurrencesObservable.subscribe((data: any[]) => {
      this.wordData = data;
    });
  }

  ngOnChanges() {
    if (this.wordCloudChart != null) {
      this.wordCloudChart.wordData = this.wordData;
      this.wordCloudChart.options = this.options;
      this.wordCloudChart.color = this.color;
      this.wordCloudChart.update();
    }
  }

}
