import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsCooccurrencesComponent } from './results-cooccurrences.component';

describe('ResultsCooccurrencesComponent', () => {
  let component: ResultsCooccurrencesComponent;
  let fixture: ComponentFixture<ResultsCooccurrencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsCooccurrencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsCooccurrencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
