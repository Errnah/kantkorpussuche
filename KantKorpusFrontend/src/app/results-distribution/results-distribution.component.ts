import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Input,
  DoCheck
} from '@angular/core';
import { SearchService } from '../search.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-results-distribution',
  templateUrl: './results-distribution.component.html',
  styleUrls: ['./results-distribution.component.css']
})

export class ResultsDistributionComponent implements OnInit, AfterViewInit, DoCheck {
  @ViewChild('canvas') canvas: ElementRef;
  @Input() hitsPerVolume: any;

  searchResults: any;
  hitsPerVolumeChart: Chart;
  labelsForVolumes = [];
  pages = [];

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.searchResultsObservable.subscribe((data: any[]) => this.searchResults = data);
    this.searchService.hitsPerVolumeObservable.subscribe((data: any[]) => this.hitsPerVolume = data);

    // get the volumes in an array for y-axes
    // tslint:disable-next-line:prefer-for-of
    for (let index = 1; index <= this.hitsPerVolume.length; index++) {
      this.labelsForVolumes.push('Bd. ' + index);
    }

    // get the page - nr of the results(sorted)
    this.searchResults.forEach(result => {
      this.pages.push(result.m_page);
      this.pages.sort((n1, n2) => n1 - n2);
    });
  }

  ngDoCheck() {
    if (this.hitsPerVolumeChart != null) {
      this.hitsPerVolumeChart.data.datasets[0].data = this.hitsPerVolume;
      this.hitsPerVolumeChart.update();
    }
  }

  ngAfterViewInit() {
    // prevents ExpressionChangedAfterItHasBeenChecked
    setTimeout(() => {
      this.chartInit();
    });
  }

  chartInit() {
    this.hitsPerVolumeChart = new Chart(
      this.canvas.nativeElement.getContext('2d'),
      {
        type: 'bar',
        data: {
          labels: this.labelsForVolumes,
          datasets: [
            {
              label: 'Treffer pro Band',
              data: this.hitsPerVolume,
              backgroundColor: 'rgba(54, 162, 235, 0.2)',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1
            }
          ]
        },
        options: {
          title: {
            text: 'Treffer Verteilung',
            display: true,
            responsive: true
          },
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true
                }
              }
            ],
            xAxes: [
              {
                ticks: {
                  beginAtZero: true
                },
                barPercentage: 0.8,
                barThickness: 'flex',
                maxBarThickness: 50,
                minBarLength: 2,
                gridLines: {
                  offsetGridLines: true
                }
              }
            ]
          }
        }
      }
    );
  }
}
