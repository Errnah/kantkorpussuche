import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsDistributionComponent } from './results-distribution.component';

describe('ResultsDistributionComponent', () => {
  let component: ResultsDistributionComponent;
  let fixture: ComponentFixture<ResultsDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsDistributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
